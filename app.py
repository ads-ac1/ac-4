from flask import Flask
app = Flask(__name__)

class App:
    @app.route('/')
    def hello():
        return '<h1>Bem vindo a AC3</h1>'

if __name__ == "__main__":
    app.run(debug=True)
